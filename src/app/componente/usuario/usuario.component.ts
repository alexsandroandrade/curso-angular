import { Usuario } from './../../model/usuario';
import { UsuarioService } from './../../service/usuario.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css'],
})
export class UsuarioComponent implements OnInit {
  usuariosList: Observable<Usuario[]>;
  login: string;
  total: number;

  constructor(private usuarioService: UsuarioService) {

  }

  ngOnInit() {
    this.usuarioService.getUsuariosList().subscribe((data) => {
      this.usuariosList = data;
    });
  }

  deleteUsuario(id: Number) {
    this.usuarioService.deletarUsuario(id).subscribe((data) => {
      console.log('retorno do metodo delete' + data);

      this.usuarioService.getUsuariosList().subscribe((data) => {
        this.usuariosList = data.content;
        this.total = data.totalElements;
      });
    });
  }

  editarUsuario(id: Number){

  }

  consultaPorLogin() {
    debugger;
    if (this.login === "") {
      this.usuarioService.getUsuariosList().subscribe((data) => {
        this.usuariosList = data;
      });
    } else {
      this.usuarioService.consultarPorNome(this.login).subscribe((data) => {
        this.usuariosList = data;
      });
    }


  }
}
