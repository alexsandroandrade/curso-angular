import { LivroService } from './../../service/livro.service';
import { Observable } from 'rxjs';
import { Livro } from './../../model/livro';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-livro',
  templateUrl: './livro.component.html',
  styleUrls: ['./livro.component.css']
})
export class LivroComponent implements OnInit {

  livrosList: Observable<Livro[]>;
  titulo: string;
  total: number;

  constructor(private livroService: LivroService) { }

  ngOnInit(): void {

    this.livroService.getLivrosList().subscribe((data) => {
      this.livrosList = data;
    });
  }

  deleteLivro(id: Number) {
    this.livroService.deletarLivro(id).subscribe((data) => {
      console.log('retorno do metodo delete' + data);

      this.livroService.getLivrosList().subscribe((data) => {
        this.livrosList = data.content;
        this.total = data.totalElements;
      });
    });
  }

  editarLivro(id: Number){

  }

  consultaPorTitulo() {

    if (this.titulo === "") {
      this.livroService.getLivrosList().subscribe((data) => {
        this.livrosList = data;
      });
    } else {
      this.livroService.consultarPorTitulo(this.titulo).subscribe((data) => {
        this.livrosList = data;
      });
    }


  }

}
