import { AppConstants } from '../app-constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LivroService {

  constructor(private http: HttpClient) {

  }

  getLivrosList(): Observable<any>{
    return this.http.get<any>(AppConstants.baseUrl + "livro/");
  }

  deletarLivro(id: Number): Observable<any> {

    return this.http.delete(AppConstants.baseUrl + id, {responseType: 'text'})
  }

  consultarPorTitulo(titulo: string): Observable<any> {

    return this.http.get(AppConstants.baseUrl + "livro/livroPorTitulo/" + titulo)
  }

}
