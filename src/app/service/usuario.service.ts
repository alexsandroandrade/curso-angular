import { AppConstants } from './../app-constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) {

  }

  getUsuariosList(): Observable<any>{
    return this.http.get<any>(AppConstants.baseUrl + "usuario/");
  }

  deletarUsuario(id: Number): Observable<any> {

    return this.http.delete(AppConstants.baseUrl + "usuario/" + id, {responseType: 'text'})
  }

  consultarPorNome(nome: string): Observable<any> {

    return this.http.get(AppConstants.baseUrl + "usuario/usuarioPorLogin/" + nome)
  }

}
