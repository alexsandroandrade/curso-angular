import { LoginServiceService } from './../service/login-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  title = 'cursoangularteste';

  usuario = {
    login: '',
    senha: ''
  }

  constructor(private loginService: LoginServiceService){
    localStorage.clear();
  }

  public login(){
     this.loginService.login(this.usuario);
  }

  ngOnInit(): void {
  }

}
