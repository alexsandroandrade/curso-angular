export class Livro {
  id: Number;
  titulo: string;
  autor: string;
  ano: number;
}
